# A Look Behind The Scenes Of React &
Optimization Techniques

### - React.memo()
React.memo for Button component will still re-render this component and print 'Button running too', because it's prop is a function which recreates for every re-render. If the props were just a primitive types, like booleans, the component wouldn't re-render.

### - useCallback()
Pass an inline callback and an array of dependencies. useCallback will return a memoized version of the callback that only changes if one of the dependencies has changed. This is useful when passing callbacks to optimized child components that rely on reference equality to prevent unnecessary renders (e.g. shouldComponentUpdate).
useCallback(fn, deps) is equivalent to useMemo(() => fn, deps).