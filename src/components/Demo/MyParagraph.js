import React from 'react';

const MyParagraph = (props) => {
    console.log('MyParagraph message');
    return (<p>{props.children}</p>);
}

export default MyParagraph;