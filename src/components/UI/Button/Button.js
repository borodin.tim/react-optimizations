import React from 'react';

import classes from './Button.module.css';

const Button = (props) => {
  console.log('Button running too');
  return (
    <button
      type={props.type || 'button'}
      className={`${classes.button} ${props.className}`}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.children}
    </button>
  );
};

// React.memo in this case will still re-render this component and print 'Button running too', because it's prop is a function which recreates for every re-render. Unless useCallback is used on the function that is passed as props
export default React.memo(Button);
